use std::collections::hash_map::Entry;
use std::collections::HashMap;

use crate::{coordinates::neighbor, inverse::Inverse};

use super::coordinates::{AxialCoordinate, Corner, Side};

pub struct HexGrid<T, S, C> {
    tiles: HashMap<AxialCoordinate, T>,
    sides: HashMap<(AxialCoordinate, Side), S>,
    corners: HashMap<(AxialCoordinate, Corner), C>,
}

/// A hexgrid storing an item of type `T` on every hexagon
impl<T, S, C> HexGrid<T, S, C> {
    pub fn new() -> Self {
        Self {
            tiles: HashMap::new(),
            sides: HashMap::new(),
            corners: HashMap::new(),
        }
    }

    pub fn entry(&mut self, key: AxialCoordinate) -> Entry<'_, AxialCoordinate, T> {
        self.tiles.entry(key)
    }

    pub fn get(&self, k: &AxialCoordinate) -> Option<&T> {
        self.tiles.get(k)
    }

    pub fn insert(&mut self, k: AxialCoordinate, v: T) -> Option<T> {
        self.tiles.insert(k, v)
    }

    pub fn side_entry(
        &mut self,
        coordinate: AxialCoordinate,
        side: Side,
    ) -> Entry<'_, (AxialCoordinate, Side), S> {
        use Side::*;
        let (coordinate_offset, side) = match side {
            TopLeft | TopRight | Right => (AxialCoordinate::default(), side),
            BottomRight | BottomLeft | Left => (side.into(), side.inverse()),
        };

        debug_assert!(matches!(side, TopLeft | TopRight | Right));

        self.sides.entry((coordinate + coordinate_offset, side))
    }

    pub fn side_get(&self, coordinate: AxialCoordinate, side: Side) -> Option<&S> {
        use Side::*;
        let (coordinate_offset, side) = match side {
            TopLeft | TopRight | Right => (AxialCoordinate::default(), side),
            BottomRight | BottomLeft | Left => (side.into(), side.inverse()),
        };

        debug_assert!(matches!(side, TopLeft | TopRight | Right));

        self.sides.get(&(coordinate + coordinate_offset, side))
    }

    pub fn side_insert(&mut self, coordinate: AxialCoordinate, side: Side, v: S) -> Option<S> {
        use Side::*;
        let (coordinate_offset, side) = match side {
            TopLeft | TopRight | Right => (AxialCoordinate::default(), side),
            BottomRight | BottomLeft | Left => (side.into(), side.inverse()),
        };

        debug_assert!(matches!(side, TopLeft | TopRight | Right));

        self.sides.insert((coordinate + coordinate_offset, side), v)
    }

    pub fn corner_entry(
        &mut self,
        coordinate: AxialCoordinate,
        corner: Corner,
    ) -> Entry<'_, (AxialCoordinate, Corner), C> {
        use Corner::*;
        let (coordinate_offset, corner) = match corner {
            Top | Bottom => (AxialCoordinate::default(), corner),
            RightUpper => (neighbor::TOP_RIGHT, Bottom),
            RightLower => (neighbor::BOTTOM_RIGHT, Top),
            LeftLower => (neighbor::BOTTOM_LEFT, Top),
            LeftUpper => (neighbor::TOP_LEFT, Bottom),
        };

        debug_assert!(matches!(corner, Top | Bottom));

        self.corners.entry((coordinate + coordinate_offset, corner))
    }

    pub fn corner_get(&self, coordinate: AxialCoordinate, corner: Corner) -> Option<&C> {
        use Corner::*;
        let (coordinate_offset, corner) = match corner {
            Top | Bottom => (AxialCoordinate::default(), corner),
            RightUpper => (neighbor::TOP_RIGHT, Bottom),
            RightLower => (neighbor::BOTTOM_RIGHT, Top),
            LeftLower => (neighbor::BOTTOM_LEFT, Top),
            LeftUpper => (neighbor::TOP_LEFT, Bottom),
        };

        debug_assert!(matches!(corner, Top | Bottom));

        self.corners.get(&(coordinate + coordinate_offset, corner))
    }

    pub fn corner_insert(
        &mut self,
        coordinate: AxialCoordinate,
        corner: Corner,
        v: C,
    ) -> Option<C> {
        use Corner::*;
        let (coordinate_offset, corner) = match corner {
            Top | Bottom => (AxialCoordinate::default(), corner),
            RightUpper => (neighbor::TOP_RIGHT, Bottom),
            RightLower => (neighbor::BOTTOM_RIGHT, Top),
            LeftLower => (neighbor::BOTTOM_LEFT, Top),
            LeftUpper => (neighbor::TOP_LEFT, Bottom),
        };

        debug_assert!(matches!(corner, Top | Bottom));

        self.corners
            .insert((coordinate + coordinate_offset, corner), v)
    }
}

impl<T, S, C> Default for HexGrid<T, S, C> {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod tests {

    use crate::inverse::Inverse;

    use super::*;

    #[test]
    fn insert_get() {
        let mut grid: HexGrid<i32, (), ()> = HexGrid::new();
        let coordinate = AxialCoordinate::default();
        assert_eq!(grid.insert(coordinate, 3), None);
        assert_eq!(grid.get(&coordinate), Some(&3));
    }

    // TODO: (quickcheck) tests for tiles

    #[quickcheck]
    fn qc_propery_insert_side_get_from_neighbor(
        coordinate: AxialCoordinate,
        side: Side,
        item: i32,
    ) -> bool {
        let mut grid: HexGrid<(), i32, ()> = HexGrid::new();

        let neighbor_coordinate = coordinate + side.into();

        grid.side_insert(coordinate, side, item);

        grid.side_get(neighbor_coordinate, side.inverse()) == Some(&item)
    }

    #[quickcheck]
    fn qc_property_neigbor_entries_point_to_same_side(
        coordinate: AxialCoordinate,
        side: Side,
        item1: i32,
        item2: i32,
        item3: i32,
    ) -> bool {
        let mut grid: HexGrid<(), i32, ()> = HexGrid::new();

        let neighbor_coordinate = coordinate + side.into();

        {
            let entry = grid.side_entry(coordinate, side).or_insert(item1);
            *entry = entry.wrapping_add(item2);
        }

        let entry2 = grid
            .side_entry(neighbor_coordinate, side.inverse())
            .or_insert(0);

        *entry2 = entry2.wrapping_add(item3);

        grid.side_get(coordinate, side) == Some(&(item1.wrapping_add(item2).wrapping_add(item3)))
    }

    // TODO: (quickcheck) tests for corners
}
