use std::ops::{Add, Sub};

/// Represents an axial coordinate.
///
/// First coordinate is Q, second is R, S is implicit.
#[derive(Copy, Clone, Debug, Hash, Eq, PartialEq, Default)]
pub struct AxialCoordinate(pub i32, pub i32);

impl Add for AxialCoordinate {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Self(self.0 + rhs.0, self.1 + rhs.1)
    }
}

impl Sub for AxialCoordinate {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Self(self.0 - rhs.0, self.1 - rhs.1)
    }
}

pub mod neighbor {
    use super::AxialCoordinate;

    pub const TOP_LEFT: AxialCoordinate = AxialCoordinate(0, -1);
    pub const TOP_RIGHT: AxialCoordinate = AxialCoordinate(1, -1);
    pub const RIGHT: AxialCoordinate = AxialCoordinate(1, 0);
    pub const BOTTOM_RIGHT: AxialCoordinate = AxialCoordinate(0, 1);
    pub const BOTTOM_LEFT: AxialCoordinate = AxialCoordinate(-1, 1);
    pub const LEFT: AxialCoordinate = AxialCoordinate(-1, 0);
}

#[derive(Copy, Clone, Debug, Hash, Eq, PartialEq)]
pub enum Side {
    TopLeft,
    TopRight,
    Right,
    BottomRight,
    BottomLeft,
    Left,
}

#[derive(Copy, Clone, Debug, Hash, Eq, PartialEq)]
pub enum Corner {
    Top,
    RightUpper,
    RightLower,
    Bottom,
    LeftLower,
    LeftUpper,
}

impl From<Side> for AxialCoordinate {
    fn from(side: Side) -> Self {
        match side {
            Side::TopLeft => neighbor::TOP_LEFT,
            Side::TopRight => neighbor::TOP_RIGHT,
            Side::Right => neighbor::RIGHT,
            Side::BottomRight => neighbor::BOTTOM_RIGHT,
            Side::BottomLeft => neighbor::BOTTOM_LEFT,
            Side::Left => neighbor::LEFT,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{neighbor, AxialCoordinate};

    #[test]
    fn coordinate_neighbor_addition() {
        assert_eq!(
            AxialCoordinate::default() + neighbor::TOP_LEFT,
            AxialCoordinate(0, -1)
        );
        assert_eq!(
            AxialCoordinate::default() + neighbor::TOP_RIGHT,
            AxialCoordinate(1, -1)
        );
        assert_eq!(
            AxialCoordinate::default() + neighbor::RIGHT,
            AxialCoordinate(1, 0)
        );
        assert_eq!(
            AxialCoordinate::default() + neighbor::BOTTOM_RIGHT,
            AxialCoordinate(0, 1)
        );
        assert_eq!(
            AxialCoordinate::default() + neighbor::BOTTOM_LEFT,
            AxialCoordinate(-1, 1)
        );
        assert_eq!(
            AxialCoordinate::default() + neighbor::LEFT,
            AxialCoordinate(-1, 0)
        );
    }

    #[test]
    fn coordinate_addition_substraction() {
        let zero = AxialCoordinate(0, 0);

        let result = zero + neighbor::RIGHT + neighbor::TOP_LEFT + neighbor::BOTTOM_LEFT;
        assert_eq!(result, zero);

        let result = zero + neighbor::LEFT + neighbor::TOP_RIGHT + neighbor::BOTTOM_RIGHT;
        assert_eq!(result, zero);
    }
}

#[cfg(test)]
mod arbitrary {

    use super::*;

    use quickcheck::Arbitrary;

    impl Arbitrary for AxialCoordinate {
        fn arbitrary(g: &mut quickcheck::Gen) -> Self {
            Self(i32::arbitrary(g) % 1000, i32::arbitrary(g) % 1000)
        }
    }

    impl Arbitrary for Side {
        fn arbitrary(g: &mut quickcheck::Gen) -> Self {
            *g.choose(&[
                Self::TopLeft,
                Self::TopRight,
                Self::Right,
                Self::BottomRight,
                Self::BottomLeft,
                Self::Left,
            ])
            .unwrap()
        }
    }

    impl Arbitrary for Corner {
        fn arbitrary(g: &mut quickcheck::Gen) -> Self {
            *g.choose(&[
                Self::Top,
                Self::RightUpper,
                Self::RightLower,
                Self::Bottom,
                Self::LeftLower,
                Self::LeftUpper,
            ])
            .unwrap()
        }
    }
}
