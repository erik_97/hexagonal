use super::coordinates::{AxialCoordinate, Corner, Side};

pub trait Inverse {
    fn inverse(&self) -> Self;
}

impl Inverse for AxialCoordinate {
    fn inverse(&self) -> Self {
        Self(0 - self.0, 0 - self.1)
    }
}

impl Inverse for Side {
    fn inverse(&self) -> Self {
        use Side::*;

        match self {
            TopLeft => BottomRight,
            TopRight => BottomLeft,
            Right => Left,
            BottomRight => TopLeft,
            BottomLeft => TopRight,
            Left => Right,
        }
    }
}

impl Inverse for Corner {
    fn inverse(&self) -> Self {
        use Corner::*;

        match self {
            Top => Bottom,
            RightUpper => LeftLower,
            RightLower => LeftUpper,
            Bottom => Top,
            LeftLower => RightUpper,
            LeftUpper => RightLower,
        }
    }
}

#[cfg(test)]
mod tests {
    use quickcheck;

    use crate::coordinates::{AxialCoordinate, Corner, Side};

    use super::Inverse;

    #[quickcheck]
    fn double_inverse_coordinate_is_original(ac: AxialCoordinate) -> bool {
        ac == ac.inverse().inverse()
    }

    #[quickcheck]
    fn move_by_inverse_brings_back_original(
        base: AxialCoordinate,
        vector: AxialCoordinate,
    ) -> bool {
        base == base + vector + vector.inverse()
    }

    #[quickcheck]
    fn double_inverse_side_is_original(s: Side) -> bool {
        s == s.inverse().inverse()
    }

    #[quickcheck]
    fn double_inverse_corner_is_original(c: Corner) -> bool {
        c == c.inverse().inverse()
    }
}
