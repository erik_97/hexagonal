//! Hexagonal grid based data structure.
//!
//! Provides an pointy topped hexagonal grid to store data in the center, side or corner.

pub mod coordinates;
pub mod grids;
pub mod inverse;

#[cfg(test)]
#[macro_use(quickcheck)]
extern crate quickcheck_macros;
